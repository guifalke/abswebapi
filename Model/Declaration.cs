﻿using System.Xml.Serialization;

namespace Model
{
    public class Declaration
    {
        [XmlAttribute]
        public string Command { get; set; }
        [XmlAttribute]
        public string Version { get; set; }
        public DeclarationHeader DeclarationHeader { get; set; }

    }
}