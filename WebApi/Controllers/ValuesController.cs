﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Microsoft.AspNetCore.Mvc;
using Model;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        [HttpGet]
        public string Get()
        {
            return "ready";
        }
        [HttpPost]
        public int Post([FromBody] InputDocument input)
        {
            try
            {
                //XmlSerializer ser = new XmlSerializer(typeof(InputDocument));
                //InputDocument input = null;

                //using (TextReader reader = new StringReader(content))
               // {
                //    input = (InputDocument)ser.Deserialize(reader);
                //}
                if (!input.DeclarationList.Declaration.Command.Equals("DEFAULT"))
                {
                    return -1;
                }
                if (!string.IsNullOrEmpty(input.DeclarationList.Declaration.DeclarationHeader.SiteID)
                    && input.DeclarationList.Declaration.DeclarationHeader.SiteID.Equals("DUB",StringComparison.OrdinalIgnoreCase))
                {
                    return 0;
                }
                else
                {
                    return -2;
                }
            }
            catch (Exception e) {
                return -1;
            }
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
